﻿using System;
using System.IO;
using System.Text;

namespace FileTest

{
    internal class Program
    {
        static void Main(string[] args)
        {
            DisplayFileNames();
            CreateTestFile();
/*            CreateTestBackup();*/

        }
        static void DisplayFileNames()
        {
            DirectoryInfo di = new DirectoryInfo(@"C:\Academy\TestFiles");
            string dirName = @"C:\Academy\TestFiles";
            if (Directory.Exists(dirName))
            {
                Console.WriteLine($"{dirName} is a valid directory");
            }
            else
            {
                Console.WriteLine($"{dirName} is an invalid directory");
            }
            
            // text files
            foreach (var fi in di.GetFiles())
            {
                Console.WriteLine(fi.Name);
            }
        }
        static void CreateTestFile() {
            string dirName = @"C:\Academy\TestFiles";
            string newFile = @"C:\Academy\TestFiles\test.txt";
            if (Directory.Exists(dirName))
            {
                using (FileStream fs = File.Create(newFile))
                {
                    byte[] info = new UTF8Encoding(true).GetBytes("Test 3.");
                    // Add some information to the file.
                    fs.Write(info, 0, info.Length);
                    Console.WriteLine($"File {newFile} created at following directory: {dirName}");
                }
                CreateTestBackup(newFile, dirName);
            }
            else
            {
                Console.WriteLine($"{dirName} is an invalid directory, could not create file {newFile}");
            }

            

        }
        static void CreateTestBackup(string currentFile, string dirName) {
            string currentDt = DateTime.Now.ToString("yyyyMMddHHmmss");
            string newFile = @"C:\Academy\TestFiles\test-backup-" + currentDt +".txt";
  
            if (!File.Exists(currentFile))
            {
                Console.WriteLine($"Error: {currentFile} doesn't exist!");
                return;
            }
          
            if (File.Exists(currentFile))
            {
                // Copy the file
                File.Copy(currentFile, newFile);
                Console.WriteLine($"Backup File {newFile} created at following directory: {dirName}");
                return;
            }


        }

    }
}